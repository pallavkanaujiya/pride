provider "aws" {
region=var.region
}

resource "aws_instance" "Main"{
ami="ami-03b8a287edc0c1253"
tenancy="default"
instance_type=var.instance
count=length(var.az)
availability_zone=var.az[count.index]
tags = {
Name="Pallav"
}
}


resource "aws_vpc" "VPC"{

cidr_block=var.vpc_block
tags= {
Name="MY_VPC"
}
}
resource "aws_subnet" "my-subnet" {
cidr_block=var.sub_block
vpc_id=aws_vpc.VPC.id
tags = {
Name= "MY-SUBNET"
}
}
output "vpc-id" {
value=aws_vpc.VPC.id
}

output "subnet-id" {
value=aws_subnet.my-subnet.id
}

