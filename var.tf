variable "region" {
default="ap-south-1"
}
#variable "access_key" {}
#variable "secret_key" {}
variable "az" {
type = list(string)
default=["ap-south-1a", "ap-south-1b" ]
}

variable "instance" {
default= "t2.micro"
}

variable "vpc_block" {
default="10.0.0.0/16"
}
variable "sub_block" {
default="10.0.1.0/24"
}
